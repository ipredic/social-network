# Social network app template

Goal of this project is to simulate Instagram using Test-driven development(TDD) approach.

`CI/CD Pipelines are processed throw Bitbucket and Code Quality checks are processed via SonarQube Cloud which is integrated into the Bitbucket repository.`

### Tech Stack

#### Client side: `React`, `TypeScript`
#### Client side app state management: `Not decided yet!`
#### Backhand: `Java11`, `SpringBoot(2.6)`, `SpringSecurity`
#### Env Test Database: `H2(In Memory)`
#### Production Database: `PostgresSQL`
#### Testing frameworks/tools: `Jest`, `React Testing library`, `JUnit5`
#### Containerization: `Docker`

## Available Scripts

## / cd client

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.