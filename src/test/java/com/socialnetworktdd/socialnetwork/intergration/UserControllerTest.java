package com.socialnetworktdd.socialnetwork.intergration;

import com.socialnetworktdd.socialnetwork.shared.GenericResponse;
import com.socialnetworktdd.socialnetwork.user.User;
import com.socialnetworktdd.socialnetwork.user.UserDTO;
import com.socialnetworktdd.socialnetwork.user.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
class UserControllerTest {

    @BeforeEach
    void cleanup() {
        userRepository.deleteAll();
    }

    @Autowired
    TestRestTemplate testRestTemplate;

    @Autowired
    UserRepository userRepository;

    @Autowired
    private ModelMapper modelMapper;

    private static final String API_V_1_USERS_CREATE = "/api/v1/users/create";

    private User createValidUser() {
        UserDTO userDto = new UserDTO();
        User user = modelMapper.map(userDto, User.class);
        user.setUsername("test-user");
        user.setDisplayName("test-display");
        user.setPassword("pass123");

        return user;
    }

    @Test
    void postUser_whenUserIsValid_receiveOk() {
        User user = createValidUser();
        ResponseEntity<Object> response = testRestTemplate.postForEntity(API_V_1_USERS_CREATE, user, Object.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void postUser_whenUserIsValid_successMessage() {
        User user = createValidUser();
        ResponseEntity<GenericResponse> response = testRestTemplate.postForEntity(API_V_1_USERS_CREATE, user, GenericResponse.class);
        assertThat(Objects.requireNonNull(response.getBody()).getMessage()).isNotNull();
    }

    @Test
    void postUser_whenUserIsValid_userSavedToDatabase() {
        User user = createValidUser();
        testRestTemplate.postForEntity(API_V_1_USERS_CREATE, user, Object.class);
        assertThat(userRepository.count()).isEqualTo(1);
    }

    @Test
    void postUser_whenUserIsValid_passwordIsHashedInDatabase() {
        User user = createValidUser();
        testRestTemplate.postForEntity(API_V_1_USERS_CREATE, user, Object.class);
        List<User> users = userRepository.findAll();
        User inDb = users.get(0);
        assertThat(inDb.getPassword()).isNotEqualTo(user.getPassword());
    }
}
