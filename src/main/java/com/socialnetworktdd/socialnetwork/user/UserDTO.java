package com.socialnetworktdd.socialnetwork.user;

import lombok.Data;

@Data
public class UserDTO {
    private long id;
    private String username;
    private String displayName;
    private String password;
}
