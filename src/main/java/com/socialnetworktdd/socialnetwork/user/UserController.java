package com.socialnetworktdd.socialnetwork.user;

import com.socialnetworktdd.socialnetwork.shared.GenericResponse;
import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/users")
public class UserController {

    private final ModelMapper modelMapper;

    private final UserService userService;

    public UserController(ModelMapper modelMapper, UserService userService) {
        this.modelMapper = modelMapper;
        this.userService = userService;
    }

    private User convertDtoToEntity(UserDTO userDTO) {
        return modelMapper.map(userDTO, User.class);
    }

    @PostMapping("/create")
    GenericResponse createUser(@RequestBody UserDTO userDto) {
        User user = convertDtoToEntity(userDto);
        userService.save(user);
        return new GenericResponse("User successfully saved");
    }
}
