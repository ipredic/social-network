type ApiEndpoints = 'signUp'

type EndpointMap = {
	// eslint-disable-next-line no-unused-vars
	[key in ApiEndpoints]: string
}

const endpoints: EndpointMap = {
	signUp: '/api/v1/users/create',
}

export default endpoints
