import axios from 'axios'

import * as apiCalls from './apiCalls'
import endpoints from './apiEndpoints'

describe('apiCalls', () => {
	describe('sign up', () => {
		it(`calls ${endpoints.signUp}`, () => {
			const mockSignup = jest.fn()
			axios.post = mockSignup
			apiCalls.signup(null)

			const path = mockSignup.mock.calls[0][0]
			expect(path).toBe('/api/v1/users/create')
		})
	})
})
