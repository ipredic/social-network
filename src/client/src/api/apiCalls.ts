import axios from 'axios'

import endpoints from './apiEndpoints'
import UserProps from '../shared/types/User'

// eslint-disable-next-line import/prefer-default-export
export const signup = (user: UserProps) => axios.post(endpoints.signUp, user)
