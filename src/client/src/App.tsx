import UserSignUpPage from 'pages/UserSignUpPage'
import * as apiCalls from './api/apiCalls'

const App = () => {
	const actions = {
		postSignup: apiCalls.signup,
	}
	return (
		<div className='App'>
			<UserSignUpPage postSignup={actions.postSignup} />
		</div>
	)
}

export default App
