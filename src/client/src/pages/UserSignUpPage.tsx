import { useState, FormEvent } from 'react'

import UserProps from '../shared/types/User'

type UserSignupProps = {
	// eslint-disable-next-line no-unused-vars,react/require-default-props
	postSignup?: (user: UserProps) => Promise<{}>
}

const UserSignUpPage = ({ postSignup = async () => ({}) }: UserSignupProps) => {
	const initValues = {
		displayName: '',
		username: '',
		password: '',
		repeatPassword: '',
	}
	const [inputValue, setInputValue] = useState<{ [x: string]: string }>(
		initValues
	)
	const [pendingApiCall, setPendingApiCall] = useState<boolean>(false)
	const handleOnChange = (event: FormEvent<HTMLInputElement>) => {
		const { name, value } = event.currentTarget

		setInputValue(prevState => ({ ...prevState, [name]: value }))
	}
	const handleClickSignup = () => {
		const user: UserProps = {
			displayName: inputValue.displayName,
			username: inputValue.username,
			password: inputValue.password,
		}
		setPendingApiCall(true)
		postSignup(user).then(r => r)
	}

	return (
		<div>
			<h1>Sign up</h1>
			<div>
				<input
					name='displayName'
					placeholder='Display name'
					value={inputValue.displayName}
					onChange={handleOnChange}
				/>
			</div>
			<div>
				<input
					name='username'
					placeholder='Username'
					value={inputValue.username}
					onChange={handleOnChange}
				/>
			</div>
			<div>
				<input
					name='password'
					type='password'
					placeholder='Password'
					value={inputValue.password}
					onChange={handleOnChange}
				/>
			</div>
			<div>
				<input
					name='repeatPassword'
					type='password'
					placeholder='Repeat password'
					value={inputValue.repeatPassword}
					onChange={handleOnChange}
				/>
			</div>
			<div>
				<button
					type='submit'
					onClick={handleClickSignup}
					disabled={pendingApiCall}
				>
					Sign up
				</button>
			</div>
		</div>
	)
}

export default UserSignUpPage
