type UserProps = {
	displayName: string
	username: string
	password: string
}

export default UserProps
