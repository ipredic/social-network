import { fireEvent, render } from '@testing-library/react'
import '@testing-library/jest-dom'

import UserSignUpPage from '../pages/UserSignUpPage'

describe('UserSignUpPage', () => {
	describe('Layout', () => {
		it('has header of Sign up', () => {
			const { container } = render(<UserSignUpPage />)
			const header = container.querySelector('h1')
			expect(header).toHaveTextContent('Sign up')
		})

		it('has input for display name', () => {
			const { queryByPlaceholderText } = render(<UserSignUpPage />)
			const displayNameInput = queryByPlaceholderText('Display name')
			expect(displayNameInput).toBeInTheDocument()
		})

		it('has input for username', () => {
			const { queryByPlaceholderText } = render(<UserSignUpPage />)
			const usernameInput = queryByPlaceholderText('Username')
			expect(usernameInput).toBeInTheDocument()
		})

		it('has input for password', () => {
			const { queryByPlaceholderText } = render(<UserSignUpPage />)
			const passwordInput = queryByPlaceholderText('Password')
			expect(passwordInput).toBeInTheDocument()
		})

		it('has password type for password input', () => {
			const { queryByPlaceholderText } = render(<UserSignUpPage />)
			const passwordInput = queryByPlaceholderText('Password')
			expect(passwordInput.type).toBe('password')
		})

		it('has input for repeat password', () => {
			const { queryByPlaceholderText } = render(<UserSignUpPage />)
			const repeatPasswordInput = queryByPlaceholderText('Repeat password')
			expect(repeatPasswordInput).toBeInTheDocument()
		})

		it('has password type for repeat password input', () => {
			const { queryByPlaceholderText } = render(<UserSignUpPage />)
			const repeatPasswordInput = queryByPlaceholderText('Repeat password')
			expect(repeatPasswordInput.type).toBe('password')
		})

		it('has sign up button', () => {
			const { container } = render(<UserSignUpPage />)
			const signUpButton = container.querySelector('button')
			expect(signUpButton).toBeInTheDocument()
		})

		describe('Form Interactions', () => {
			const changeEvent = content => ({
				target: {
					value: content,
				},
			})

			let displayNameInput
			let usernameInput
			let passwordInput
			let passwordRepeat
			let button

			const setupForSubmit = props => {
				const rendered = render(<UserSignUpPage {...props} />)
				const { container, queryByPlaceholderText } = rendered

				displayNameInput = queryByPlaceholderText('Display name')
				usernameInput = queryByPlaceholderText('Username')
				passwordInput = queryByPlaceholderText('Password')
				passwordRepeat = queryByPlaceholderText('Repeat password')

				fireEvent.change(displayNameInput, changeEvent('my-display-name'))
				fireEvent.change(usernameInput, changeEvent('my-user-name'))
				fireEvent.change(passwordInput, changeEvent('admin12'))
				fireEvent.change(passwordRepeat, changeEvent('admin12'))

				button = container.querySelector('button')

				return rendered
			}

			const mockAsyncDelay = () =>
				jest.fn().mockImplementation(
					() =>
						new Promise(resolve => {
							setTimeout(() => {
								resolve({})
							}, 300)
						})
				)

			it('sets display name value into state', () => {
				const { queryByPlaceholderText } = render(<UserSignUpPage />)
				displayNameInput = queryByPlaceholderText('Display name')

				fireEvent.change(displayNameInput, changeEvent('my-display-name'))
				expect(displayNameInput).toHaveValue('my-display-name')
			})

			it('sets username value into state', () => {
				const { queryByPlaceholderText } = render(<UserSignUpPage />)
				usernameInput = queryByPlaceholderText('Username')

				fireEvent.change(usernameInput, changeEvent('my-username'))
				expect(usernameInput).toHaveValue('my-username')
			})

			it('calls postSignup when the fields are valid and the actions are provided in props', () => {
				const actions = {
					postSignup: jest.fn().mockResolvedValueOnce({}),
				}

				setupForSubmit({ ...actions })

				fireEvent.click(button)
				expect(actions.postSignup).toHaveBeenCalledTimes(1)
			})

			it('does not throw exception when button is clicked when actions are not provided', () => {
				setupForSubmit({})
				expect(() => fireEvent.click(button)).not.toThrow()
			})

			it('calls post with user body when the fields are valid', () => {
				const actions = {
					postSignup: jest.fn().mockResolvedValueOnce({}),
				}
				setupForSubmit({ ...actions })
				fireEvent.click(button)
				const expectedUserObject = {
					displayName: 'my-display-name',
					username: 'my-user-name',
					password: 'admin12',
				}
				expect(actions.postSignup).toHaveBeenCalledWith(expectedUserObject)
			})

			it('does not allow user to click SignUp button when there is outgoing api calls', () => {
				const actions = {
					postSignup: mockAsyncDelay(),
				}

				setupForSubmit({ ...actions })
				fireEvent.click(button)
				fireEvent.click(button)

				expect(actions.postSignup).toBeCalledTimes(1)
			})
		})
	})
})
